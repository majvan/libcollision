Utility for searching (possible) duplicate symbols in shared objects.

The purpose of the script is to discover potential issues in the deployment
when a product (usually shared library) redefines symbols which customer's
application expect to be defined in other shared library.

The script reads binary for its defined symbols and follows the links to other
dependent libraries. The link to the libraries is resolved by ld.so.
Found symbols are collected into internal pool.
Typical usage of this script is to run it on specific binary with addition of
another (potential) library to be occured in the system.

The python3 interpreter is required to run the script.
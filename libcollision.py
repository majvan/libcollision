#!/bin/env python3
""" The script parses the elf binaries for the symbols recursively with depending libraries.
The table with symbols is constructed and duplicates are displayed.
The table can be extended with another libraries.
"""
import subprocess
import re
import logging
import io
import sys

__version__ = '0.1.0'
__author__ = 'Juraj Vanco'
__author_email__ = 'majvan@gmail.com'
__url__ = 'https://gitlab.com/majvan/libcollision'
__license__ = 'Apache 2.0'

HELP_MSG = """
Utility for searching (possible) duplicate symbols in shared objects.
Version {}
The purpose of the script is to discover potential issues in the deployment
when a product (usually shared library) redefines symbols which customer's
application expect to be defined in other shared library.
The script reads binary for its defined symbols and follows the links to other
dependent libraries. The link to the libraries is resolved by ld.so.
Found symbols are collected into internal pool.
Typical usage of this script is to run it on specific binary with addition of
another (potential) library to be occured in the system.
Use {} [options] binary[ binary[ ...]]
Options:
-e, --exclude binary[ binary[ ...]]
	Exclude file 'binary' to be searched for the symbols and dependencies
-i, --include binary[ binary[ ...]]
	Include file 'binary' to be searched for the symbols and dependencies
	This switch is set by default
binary
	Include file 'binary' to be seached for the symbols and dependencies
""".format(__version__, sys.argv[0])

include_libs = []
exclude_libs = ["linux-vdso.so.1", "not found", "statically linked", ]
symbols = {}
libs = {}
todo_libs = set()

class ObjParserError(BaseException):
	def __init__(self, msg):
		self.msg = msg
	def __str__(self):
		return self.msg

class Symbol(object):
	""" Symbol storing information:
            - name
            - where implemented (list with debug data)
            - where referenced (list with debug data)
        """

	def __init__(self, symbol_name):
		self.name = symbol_name
		self.required_by = []
		self.implemented_by = []

	def required(self, elf, elements={}, original_line=""):
		""" Add the info about reference to this symbol.
		    parsed_elements: how the symbol was parsed from the command output
		    original_line: original line referencing the line to parse
                """
		if self.name == "":
			raise AssertionError("Illegal symbol name")
		for (i1, i2, i3) in self.required_by:
			if i1 == elf and i3 == original_line:
				#already there, return
				return
		to_add = (elf, elements, original_line)
		self.required_by.append(to_add)

	def implemented(self, elf, elements={}, original_line=""):
		""" Add the info about implementation to this symbol
		    parsed_elements: how the symbol was parsed from the command output
		    original_line: original line referencing the line to parse
		"""
		if self.name == "":
			raise AssertionError("Illegal symbol name")
		for (i1, i2, i3) in self.implemented_by:
			if i1 == elf and i3 == original_line:
				#already there, return
				return
		to_add = (elf, elements, original_line)
		self.implemented_by.append(to_add)

	def merge(self, other_symbol):
		""" Merge the other_symbol data into this symbol. Omits the other_symbol name """
		for (i1, i2, i3) in other_symbol.required_by:
			for (t1, t2, t3) in self.required_by:
				if t1 == i1 or t3 == i3:
					break
			else:
				self.required_by.append((i1, i2, i3))
		for (i1, i2, i3) in other_symbol.implemented_by:
			for (t1, t2, t3) in self.implemented_by:
				if t1 == i1 or t3 == i3:
					break
			else:
				self.implemented_by.append((i1, i2, i3))
class SymbolPool(object):
	""" Pool of symbols. This pool has double functionality: 
	    - symbol factory through get() method
	    - pool storage of symbols that belong together
        """

	def __init__(self):
		self.pool = {}

	def get(self, symbol_name=""):
		""" Get a symbol object from pool with specific name or any new unnamed symbol """
		if symbol_name == "":
			return Symbol()
		else:
			return self.pool[symbol_name]

	def add(self, symbol):
		""" Add symbol object to the pool """
		if symbol.name not in self.pool.keys():
			#new unnamed symbol
			self.pool[symbol.name] = symbol
		else:
			#get already stored symbol and merge => update with new data
			s = self.pool[symbol.name]
			s.merge(symbol)

	def merge(self, other_pool):
		""" Add symbols from other pool """
		for symbol in other_pool.pool.values():
			if symbol.name not in self.pool.keys():
				#new symbol
				self.pool[symbol.name] = symbol
			else:
				#update our symbol with new data
				self.pool[symbol.name].merge(symbol)

class Binary(object):

	def __init__(self, path):
		self.path = path
		self.symbols = SymbolPool()
		self.required_by = []
		self.symbols_resolved = False
		self.libs_resolved = False

	def search_symbols(self):
		""" Methods to search the library for the symbols """
		#get elf file format
		cmd = ['objdump', '-G', self.path]
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
		for line in io.TextIOWrapper(p.stdout, encoding="utf-8"):
			if len(line) > 1:
				break
		else:
			raise ObjParserError("Could not read the info from 'objdump -G {}'".format(self.path))
		self.format = line.split("file format")[-1].strip()
		#parse the symbol table
		cmd = ['objdump', '-T', self.path]
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
		for line in io.TextIOWrapper(p.stdout, encoding="utf-8"):
			original_line = line = line.strip()
        
			elements = {}
			if self.format == 'elf64-x86-64':
				try:
					elements['addr'] = int(line[0:16].strip(), 16) #addr takes first 16 chars, try to resolve the numeric address
				except ValueError:
					continue #next line, we consider this as a garbage
				elements['flag0'] = line[17:21].strip() #flag0 takes next 4 chars
				elements['flag1'] = line[22:24].strip() #flag1 takes next 2 chars
				elements['segment'], line = line[25:].split('\t') #segment takes next data up to tab
				try:
					elements['size'] = int(line[0:16].strip(), 16) #size takes next 16 chars, try to resolve the numeric address
				except ValueError:
					continue #next line, we consider this as a garbage
        
				line, elements['symbol'] = line[16:].rsplit(' ', 1) #symbol takes last data before space
				elements['lib'] = line.strip() #lib takes everything left before symbol
			else:
				raise ObjParserError("The binary has unknown format '{}'".format(self.format))
				#never tested, but this could be for 'elf-x86' format maybe?
				elements['addr'] = line[0:8].strip()
				elements['flag0'] = line[9:13].strip()
				elements['flag1'] = line[14:16].strip()
			if 'l' in elements['flag0']:
				#local symbol
				continue
			if 'd' in elements['flag0']:
				#debug symbol
				continue
			if 'F' in elements['flag1']:
				#function symbol
				pass
			elif 'O' in elements['flag1']:
				#object symbol
				pass
			else:
				#nothing else interesting
				continue
			if elements['segment'] == '*ABS*':
				if elements['lib'] == elements['symbol']:
					#definition of library + version
					continue
			if elements['segment'] == '*UND*' and 'w' in elements['flag0']:
				#https://docs.oracle.com/cd/E19683-01/816-1386/chapter2-11/index.html
				continue
			s = Symbol(elements['symbol'])
			if elements['segment'] == '*UND*':
				s.required(self.path, elements, original_line)
			else:
				s.implemented(self.path, elements, original_line)
			self.symbols.add(s)
		self.symbols_resolved = True

	def get_symbols(self):
		""" Returns the symbol pool from this library after search """
		return self.symbols

	def search_libs(self, pool):
		""" Returns the dependend libraries on this binary """
		cmd = ['ldd', '-d', self.path]
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
		for line in io.TextIOWrapper(p.stdout, encoding="utf-8"):
			#first remove the (0x...)
			line = line.rsplit(' (', 1)[0]
			#then get library + version only
			elements = line.strip().split('=>')
			if len(elements) >= 2:
				lib_name = elements[0].strip()
				path = elements[1].strip()
			else:
				lib_name = elements[0].rsplit('/')[-1].strip()
				path = elements[0].strip()
			bin = pool.get(path)
			bin.required_by.append(self)
			pool.add(bin) #add new library to the same pool
		self.libs_resolved = True

class BinaryPool(object):
	""" Pool of binaries (mostly libraries) """

	def __init__(self, excluded_bins=[]):
		""" Initialize the pool. Any lib with the path from excluded bins cannot be added into pool.
		"""
		self.excluded_bins = excluded_bins
		self.pool = {}

	def get(self, bin_path=""):
		""" Get a binary object from pool with specific path or any new initialized binary """
		if bin_path not in self.pool.keys():
			return Binary(bin_path)
		else:
			return self.pool[bin_path]

	def add(self, bin):
		""" Add binary object to the pool.
		We are not currently allowing merge of binaries with the same path. 
		Instead we presume that if anyone wants to add the binary with the same
		name as it was already added in the pool, then it must be the same object.
		"""
		if bin.path == "":
			return AssertionError("The binary has not path.")
		if bin.path in self.excluded_bins:
			return
		if bin not in self.pool.values():
			#new binary
			self.pool[bin.path] = bin

	def get_unresolved(self):
		""" Gets first unresolved binary or None if all binaries are resolved.
		    This algorithm is not optimal, but it is robust.
		    We go through all the binaries recursively in the pool to find the first uninitialized one.
		"""
		rv = None
		for b in self.pool.values():
			if not b.symbols_resolved:
				rv = b
				break
		return rv

if __name__ == "__main__":

	def parse_cmdline():
		""" Parses the command line into global include and exclude dir variables """
		state = ["INCLUDE", "-"]
		retval = True
		
		if len(sys.argv) <= 1:
			print(HELP_MSG)
			retval = False
		else:
			for arg in sys.argv[1:]:
				if arg[0] == '-' and state[1] == "FILE":
					print("Error: {} is not file.".format(arg))
					retval = False
					break
				elif arg == "-h" or arg == "--help":
					print(HELP)
					retval = False
					break
				elif arg == "-x" or arg == "--exclude":
					state = ["EXCLUDE", "FILE"]
				elif arg == "-i" or arg == "--include":
					state = ["INCLUDE", "FILE"]
				else:
					if state[0] == "INCLUDE":
						include_libs.append(arg)
					else: #state[0] == "EXCLUDE"
						exclude_libs.append(args)
					state[1] = "-"
		return retval

	if not parse_cmdline():
		sys.exit(-1)
	#put all the executables into starter pool
	bins = BinaryPool(exclude_libs)
	symbols = SymbolPool()
	#put all the libraries and executables into starter pool
	for file in include_libs:
		b = Binary(file)
		bins.add(b)
	while True:
		b = bins.get_unresolved()
		if b is None:
			break
		b.search_libs(bins) #retrieve next libs that this binary needs and add to the common pool
		b.search_symbols()
		symbols.merge(b.get_symbols()) #move the retrieved symbols to the common symbol pool
	
	#Display results. For this we need detailed info from parsed lines for symbols
	line = 0
	for symbol in symbols.pool.values():
		ib = symbol.implemented_by
		if len(ib) > 1: #issue is when at least 1 is global, i.e. more than 1 is implementing symbol
			num_of_globals = 0
			#issue is NOT when all the referenced libraries is the only one library
			libs = set() #get unique libs in set
			for implementer, how, source in ib: #get the detailed info about symbols
				libs.add(implementer)
				if 'g' in how['flag0']:
					num_of_globals += 1
			if len(libs) == 1:
				continue
			if num_of_globals <= 1:
				continue
			#else print the symbol and print the detailed info for each implementer
			if line == 0:
				print("The conflicting symbols are:")
			line += 1
			print(symbol.name)
			for implementer, how, source in ib:
				print("\t{}\n\t{}".format(implementer, source))
	if line == 0:
		print("No conflicting symbols found.")
